## Exercício 1 ##

    ## Exercício 1 - Teste de API ##

         ## Comando - Post ##

[
  {
    "id": 1,
    "ana.maia": "97 110 97 46 109 97 105 97",
    "Ana": "65 110 97",
    "Maia": "77 97 105 97",
    "ana.maia@gmail.com": "97 110 97 46 109 97 105 97 64 103 109 97 105 108 46 99 111 109",
    "ana.maia1122": "97 110 97 46 109 97 105 97 49 49 50 50",
    "551197455-0815": "53 53 49 49 57 55 52 53 53 53 45 48 56 49 53",
    "userStatus": 1
  }
]

[
  {
    "id": 2,
    "rodrigo.mendes": "114 111 100 114 105 103 111 46 109 101 110 100 101 115",
    "Rodrigo": "82 111 100 114 105 103 111",
    "Mendes": "77 101 110 100 101 115",
    "rodrigo.mendes@gmail.com": "114 111 100 114 105 103 111 46 109 101 110 100 101 115 64 103 109 97 105 108 46 99 111 109",
    "rodrigo.mendes2233": "114 111 100 114 105 103 111 46 109 101 110 100 101 115 50 50 51 51",
    "551195423-4599": "53 53 49 49 57 53 52 50 51 45 52 53 57 57",
    "userStatus": 1
  }
]

[
  {
    "id": 3,
    "tatiana.vasconcelos": "116 97 116 105 97 110 97 46 118 97 115 99 111 110 99 101 108 111 115",
    "Tatiana": "84 97 116 105 97 110 97",
    "Vasconcelos": "86 97 115 99 111 110 99 101 108 111 115",
    "tatiana.vasconcelos@gmail.com": "116 97 116 105 97 110 97 46 118 97 115 99 111 110 99 101 108 111 115 64 103 109 97 105 108 46 99 111 109",
    "tatiana.vasconcelos3344": "116 97 116 105 97 110 97 46 118 97 115 99 111 110 99 101 108 111 115 51 51 52 52",
    "551197513-4400": "53 53 49 49 57 55 53 49 51 52 52 48 48",
    "userStatus": 1
  }
]

[
"id": 4,
  "category": {Dog}
    "id": 4.1,
    "Snoopy": "83 110 111 111 112 121"
  },
  "Snoopy": "dog",
  "photoUrls": [
    "0000"
  ],
  "tags": [
    {
      "id": 4.2,
      "Snoopy": "83 110 111 111 112 121"
    }
  ],
  "status": "available"
  [
  {
  "id": 5,
  "category": {Cat}
    "id": 5.1,
    "Bichento": "66 105 99 104 101 110 116 111"
  },
  "Bichento": "cat",
  "photoUrls": [
    "0000"
  ],
  "tags": [
    {
      "id": 5.2,
      "Bichento": "66 105 99 104 101 110 116 111"
    }
  ],
  "status": "available"
}
[
{
  "id": 6,
  "category": {Platypus}
    "id": 6.1,
    "Perry": "80 101 114 114 121"
  },
  "Perry": "platypus",
  "photoUrls": [
    "0000"
  ],
  "tags": [
    {
      "id": 6.2,
      "Perry": "80 101 114 114 121"
    }
  ],
  "status": "available"
}
[
  {
    "id": 4,
    "category": {Dog}
      "id": 4.1,
      "Snoopy": "83 110 111 111 112 121"
    },
    "Snoopy": "dog",
    "photoUrls": [
      "0000"
    ],
    "tags": [
      {
        "id": 4.2,
        "Snoopy": "83 110 111 111 112 121"
      }
    ],
    "status": "available"
  }
]
{
  "id": 7,
  "petId": 4,
  "quantity": 1,
  "shipDate": "2020-07-07T17:50:12.009Z",
  "status": "approved",
  "complete": true
}
[
  {
    "id": 5,
    "category": {Cat}
      "id": 5.1,
      "Bichento": "66 105 99 104 101 110 116 111"
    },
    "Bichento": "cat",
    "photoUrls": [
      "0000"
    ],
    "tags": [
      {
        "id": 5.2,
        "Bichento": "66 105 99 104 101 110 116 111"
      }
    ],
    "status": "available"
  }
]
{
  "id": 7,
  "petId": 5,
  "quantity": 1,
  "shipDate": "2020-08-07T15:30:12.009Z",
  "status": "delivered",
  "complete": true
}
[
  {
    "id": 6,
    "category": {Platypus}
      "id": 6.1,
      "Perry": "80 101 114 114 121"
    },
    "Perry": "platypus",
    "photoUrls": [
      "0000"
    ],
    "tags": [
      {
        "id": 6.2,
        "Perry": "80 101 114 114 121"
      }
    ],
    "status": "available"
  }
]
{
  "id": 7,
  "petId": 6,
  "quantity": 1,
  "shipDate": "2020-09-07T12:47:07.009Z",
  "status": "approved",
  "complete": true
}


## Exercício 2 - Teste Web ##

    ## Comandos em Selenium IDE ##

        ## Comando de busca pelos veículos e Script de Verificação ##

1
open	/principal/index.jsp		
2
set window size	1296x1000		
3
click	id=anunciosNovos		
4
click	css=.hero--overlay		
5
mouse over	css=.col-xs-6:nth-child(1) .btn		
6
click	css=.col-xs-6:nth-child(1) .btn		
7
mouse out	css=.open > .btn		
8
type	css=.open > .dropdown-menu .form-control	rena	
9
click	linkText=Renault		
10
select	id=sltMake	label=Renault	
11
mouse over	css=.col-xs-3:nth-child(1) .btn		
12
mouse out	css=.col-xs-3:nth-child(1) .btn		
13
click	css=.hero--overlay		
14
mouse over	css=.col-xs-6:nth-child(2) .btn		
15
click	css=.col-xs-6:nth-child(2) .btn		
16
mouse out	css=.open > .btn		
17
type	css=.open > .dropdown-menu .form-control	sandero	
18
click	css=.open > .dropdown-menu > .dropdown-menu > li:nth-child(25) .text		
19
select	id=sltModel	label=Sandero Stepway	
20
click	css=.hero--overlay		
21
mouse over	css=.col-xs-3:nth-child(1) .btn		
22
click	css=.col-xs-3:nth-child(1) .btn		
23
mouse out	css=.open > .btn		
24
click	linkText=De 2019		
25
select	id=sltYearMin	label=De 2019	
26
click	css=.hero--overlay		
27
mouse over	css=.button--large		
28
click	css=.button--large		
29
click	css=.primeiro:nth-child(3)		
30
run script	window.scrollTo(0,100)		
31
click	css=#ac30078410 .esquerda		
32
click	css=.sectionbackground:nth-child(1)		
33
click	css=a use		
34
click	css=.hero--overlay		
35
click	id=anunciosNovos		
36
click	css=.col-xs-6:nth-child(1) .filter-option		
37
mouse over	css=.col-xs-6:nth-child(1) .filter-option		
38
mouse out	css=.open .filter-option		
39
type	css=.open > .dropdown-menu .form-control	ren	
40
click	css=.active .text		
41
mouse over	css=.open > .btn		
42
select	id=sltMake	label=Renault	
43
mouse over	css=.col-xs-3:nth-child(1) .btn		
44
mouse out	css=.col-xs-3:nth-child(1) .btn		
45
type	css=.open > .dropdown-menu .form-control	sande	
46
click	css=.open > .dropdown-menu > .dropdown-menu > li:nth-child(25) .text		
47
select	id=sltModel	label=Sandero Stepway	
48
click	css=.col-xs-3:nth-child(1) .filter-option		
49
mouse over	css=.col-xs-3:nth-child(1) .filter-option		
50
mouse out	css=.open .filter-option		
51
mouse over	css=.open > .btn		
52
click	css=.col-xs-3:nth-child(1) .btn		
53
select	id=sltYearMin	label=De 2018	
54
click	css=.hero--overlay		
55
click	css=.button--large		
56
run script	window.scrollTo(0,114)		
57
run script	window.scrollTo(0,385)		
58
click	css=#ac30078438 .esquerda		
59
click	css=.sectionbackground:nth-child(1)		
60
click	css=.sectionbackground:nth-child(1)		
61
click	css=a > .container-logo		
62
click	css=.hero--overlay		
63
click	id=anunciosNovos		
64
click	css=.col-xs-6:nth-child(1) .filter-option		
65
mouse over	css=.col-xs-6:nth-child(1) .filter-option		
66
mouse out	css=.open .filter-option		
67
type	css=.open > .dropdown-menu .form-control	ren	
68
click	css=.active .text		
69
mouse over	css=.open > .btn		
70
select	id=sltMake	label=Renault	
71
mouse over	css=.col-xs-3:nth-child(1) .btn		
72
mouse out	css=.col-xs-3:nth-child(1) .btn		
73
type	css=.open > .dropdown-menu .form-control	sande	
74
click	css=.open > .dropdown-menu > .dropdown-menu > li:nth-child(25) .text		
75
select	id=sltModel	label=Sandero Stepway	
76
click	css=.hero--overlay		
77
click	css=.col-xs-3:nth-child(1) .filter-option		
78
mouse over	css=.col-xs-3:nth-child(1) .filter-option		
79
mouse out	css=.open .filter-option		
80
mouse over	css=.open > .btn		
81
click	css=.col-xs-3:nth-child(1) .btn		
82
select	id=sltYearMin	label=De 2017	
83
click	css=.button--large		
84
run script	window.scrollTo(0,653)		
85
run script	window.scrollTo(0,1043)		
86
click	css=#ac30125976 .usado > p

    ## Base de Dados dos Veículos Criada Manualmente em Excel ##